package main;

import javax.swing.JFrame;

import states.MenuState;
import states.PlayState;

public class Game {
	public static final int MENU_STATE = 0;
	public static final int PLAY_STATE = 1;
	
	public static GamePanel gamePanel;
	
	public static void main(String[] args) {
		JFrame window = new JFrame("Restless Phantasm");
		gamePanel = new GamePanel();
		
		window.setContentPane(gamePanel);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setResizable(false);
		window.setLocationRelativeTo(null);
		
		gamePanel.addState(new MenuState(MENU_STATE));
		gamePanel.addState(new PlayState(PLAY_STATE));
		gamePanel.setStateById(MENU_STATE);
		gamePanel.init();
		
		window.pack();
		window.setVisible(true);

		gamePanel.run();
	}
}