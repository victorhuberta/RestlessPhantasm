package utils;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class Animation {
	private ArrayList<BufferedImage> frames;
	private int currentFrameIndex;
	private int initialFrameRepeat;
	private int frameRepeat;
	
	public Animation() {
		frames = new ArrayList<BufferedImage>();
		currentFrameIndex = 0;
	}
	
	public ArrayList<BufferedImage> getFrames() {
		return frames;
	}
	
	public static ArrayList<String> getAllImageTitles
		(String baseName, String ext, int count) {
	
		ArrayList<String> imageTitles = new ArrayList<>();
		
		for (int i = 1; i <= count; ++i) {
			imageTitles.add(baseName + i + ext);
		}
		
		return imageTitles;
	}
	
	public void setFrames(ArrayList<String> imageTitles, int initialFrameRepeat) {
		for (String imageTitle : imageTitles) {
			try {
				BufferedImage image = ImageIO.read(getClass()
					.getResourceAsStream(imageTitle));
				frames.add(image);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		this.initialFrameRepeat = initialFrameRepeat;
		this.frameRepeat = initialFrameRepeat;
	}
	
	public void updateCurrentFrame() {
		if (frameRepeat == 0) {
			frameRepeat = initialFrameRepeat;
			currentFrameIndex++;
			if (currentFrameIndex >= frames.size()) 
				currentFrameIndex = 0;
				
		} else {
			frameRepeat--;
		}
	}
	
	public BufferedImage getCurrentFrame() {
		return frames.get(currentFrameIndex);
	}
	
	public int getCurrentFrameIndex() {
		return currentFrameIndex;
	}
}