package states;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public abstract class GameState {
	protected int id;
	
	public GameState(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public abstract void init();
	public abstract void update();
	public abstract void render(Graphics2D g);
	public abstract void keyPressed(KeyEvent e);
	public abstract void keyReleased(KeyEvent e);
	public abstract void mouseClicked(MouseEvent e);
}