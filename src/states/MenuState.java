package states;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import entities.*;
import main.Game;
import utils.GameAudio;

public class MenuState extends GameState {
	private static final String MENU_BG_RES = "/backgrounds/menu_bg.jpg";
	private static final String TITLE1_RES = "/backgrounds/title1.png";
	private static final String TITLE2_RES = "/backgrounds/title2.png";
	private static final String START_BUTTON_RES = "/buttons/start.png";
	private static final String EXIT_BUTTON_RES = "/buttons/exit.png";
	
	private GameAudio audio;
	private GameAudio soundAudio;
	private Background bg;
	private Sprite title1;
	private Sprite title2;
	private GameButton startButton;
	private GameButton exitButton;
	
	public MenuState(int id){
		super(id);
		bg = new Background(MENU_BG_RES);
		title1 = new Sprite(TITLE1_RES);
		title2 = new Sprite(TITLE2_RES);
		startButton = new GameButton(START_BUTTON_RES, () -> {
			Game.gamePanel.setStateById(Game.PLAY_STATE);
		});
		exitButton = new GameButton(EXIT_BUTTON_RES, () -> {
			System.exit(0);
		});
		
		audio = new GameAudio();
		soundAudio = new GameAudio();
	}

	@Override
	public void init() {
		title1.setPos(70, 30);
		title2.setPos(120, 135);
		startButton.setPos(100, 400);
		startButton.setScale(0.5);
		exitButton.setPos(100, 450);
		exitButton.setScale(0.5);
	}

	@Override
	public void update() {
		if (! audio.isPlaying())
			audio.playAudio(GameAudio.MENU_AUDIO_RES);
	}

	@Override
	public void render(Graphics2D g) {
		bg.render(g);
		title1.render(g);
		title2.render(g);
		startButton.renderScaled(g);
		exitButton.renderScaled(g);
	}

	@Override
	public void keyPressed(KeyEvent e) {}

	@Override
	public void keyReleased(KeyEvent e) {}

	@Override
	public void mouseClicked(MouseEvent e) {
		soundAudio.playAudio(GameAudio.BUTTON_AUDIO_RES);

		if (startButton.isClicked(e)) {
			audio.stopPlaying();
			startButton.handle();
		} else if (exitButton.isClicked(e)) {
			audio.stopPlaying();
			exitButton.handle();
		}
	}
}