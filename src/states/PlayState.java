package states;

import java.awt.*;
import java.awt.event.*;

import javax.swing.Timer;

import entities.*;
import managers.*;
import utils.GameAudio;

public class PlayState extends GameState implements ActionListener {
	// Score & its fonts.
	private Integer score;
	private Font regularFont;
	private Color fontColor;
	
	// Game play acceleration.
	private static final int BASE_ACCEL_DELAY = 300;
	private static final double SPEED_MULT_LIMIT = 1.06;
	private int accelDelay;
	private double speedMult;

	private boolean engageMode;
	private Timer engageTimer;
	
	// Background-related variables.
	private static final String PLAY_BG_RES = "/backgrounds/play_bg.png";
	private static final int INITIAL_BG_VELOCITY_X = -5;
	private Background bg;
	
	// Sprite managers.
	private PlayerManager pm;
	private ArrowKeyManager akm;
	private EnemyManager em;
	private ObstacleManager om;
	private DialogManager dm;
	
	private boolean blinkToggle;
	
	private GameAudio gamePlayAudio;
	private GameAudio soundAudio;
	private GameAudio bossFightAudio;
	private GameAudio gameOverAudio;
	
	public PlayState(int id) {
		super(id);
		
		score = 0;
		regularFont = new Font("Comic Sans", Font.BOLD, 20);
		fontColor = new Color(146, 40, 134);
		
		bg = new Background(PLAY_BG_RES);

		pm = new PlayerManager();
		akm = new ArrowKeyManager();
		em = new EnemyManager();
		om = new ObstacleManager();
		dm = new DialogManager();
		
		blinkToggle = false;
		
		gamePlayAudio = new GameAudio();
		soundAudio = new GameAudio();
		bossFightAudio = new GameAudio();
		gameOverAudio = new GameAudio();
		
		accelDelay = BASE_ACCEL_DELAY;
		speedMult = 1;
		
		engageMode = false;
		engageTimer = new Timer(2000, this);
	}
	
	@Override
	public void init() {
		bg.setPos(0, 0);
		bg.setVelocity(INITIAL_BG_VELOCITY_X, 0);
	}
	
	@Override
	public void update() {
		if (pm.isPlayerDead()) {
			updateGameOver();
			return;
		}
		
		score++;
		
		bg.update();
		pm.update();
		
		accelerateUntilSpeedLimit();
		startEngageModeWhenSpeedLimitIsReached();
		updateNonEngageMode();
		updateEngageMode();
		
		om.removeDeadObstacles();
		om.updateObstaclesAndCheckCollision(pm.getPlayer());
	}
	
	public void updateGameOver() {
		if (! pm.isPlayerDead()) return;
	
		if (! gameOverAudio.isPlaying()) {
			gamePlayAudio.stopPlaying();
			bossFightAudio.stopPlaying();
			gameOverAudio.playAudio(GameAudio.GAME_OVER_AUDIO_RES);
		}
		
		bg.setPos(0, 0);
		bg.setVelocity(0, 0);
		dm.updateDialog(DialogManager.GAME_OVER);
	}
	
	private void accelerateUntilSpeedLimit() {
		if (speedMult < SPEED_MULT_LIMIT) {
			accelDelay--;
			if (accelDelay <= 0) {
				speedMult += 0.01;
				accelDelay = BASE_ACCEL_DELAY;
				
				bg.accelerateX(speedMult);
				
				om.accelerateObstaclesBy(speedMult);
			}
		}
	}
	
	private void startEngageModeWhenSpeedLimitIsReached() {
		if (speedMult >= SPEED_MULT_LIMIT) {
			if (! engageMode)
				initEngageMode();
		}
	}
	
	private void updateNonEngageMode() {
		if (engageMode) return;
		
		if (! gamePlayAudio.isPlaying()) {
			bossFightAudio.stopPlaying();
			gamePlayAudio.playAudio(GameAudio.GAMEPLAY_AUDIO_RES);
		}
		
		om.spawnObstaclesWhenItIsTime();
	}
	
	private void initEngageMode() {
		engageMode = true;
		pm.getPlayer().resetHp();
		
		om.killAllObstacles();
		
		pm.dragPlayerToSpecificPosition();
		
		em.setEnemiesPos();
		em.randomizeEnemy();
		em.dragEnemyToSpecificPosition();
		
		gamePlayAudio.stopPlaying();
		if (em.getCurrentEnemy().getImgSrc().equals(EnemyManager.ENEMY0_RES))
			soundAudio.playAudio(GameAudio.GHOST_AUDIO_RES);
		else
			soundAudio.playAudio(GameAudio.BEAST_AUDIO_RES);
		
		engageTimer.start();
	}
	
	private void updateEngageMode() {
		if (! engageMode) return;
		
		if (! bossFightAudio.isPlaying())
			bossFightAudio.playAudio(GameAudio.BOSS_FIGHT_AUDIO_RES);
		
		pm.adjustPlayerPosIfWithinRangeFromDest();
		em.adjustEnemyPosIfWithinRangeFromDest();
		
		if (! em.updateEnemy()) {
			engageMode = false;
			return;
		}
		akm.updateAllArrowKeys();
		
		if (akm.hasCurrentSetBeenCompleted()) {
			Enemy currentEnemy = em.getCurrentEnemy();
			
			currentEnemy.damaged(pm.getPlayer().getAtk());
			em.burnEnemy();
			
			if (! currentEnemy.isAlive) {
				engageTimer.stop();
				
				if (em.getCurrentEnemy().getImgSrc().equals(EnemyManager.ENEMY0_RES))
					soundAudio.playAudio(GameAudio.GHOST_AUDIO_RES);
				else
					soundAudio.playAudio(GameAudio.BEAST_AUDIO_RES);
				
				speedMult = 1;
				em.isFlamed = false; // Undo the flame.
				em.explodeEnemy();
				
				pm.getPlayer().resetHp();
				
				// Reset the background velocity.
				bg.setVelocity(INITIAL_BG_VELOCITY_X, 0);
				
				// Make all enemies live longer.
				em.setCurrentEnemiesHp(em.getCurrentEnemiesHp() + 1);
				currentEnemy.isAlive = true;
				
				// Countdown to when the arrow keys number should
				// be incremented by one.
				akm.keysIncCountdown();
			}
			
			akm.reset();
		}
	}
	
	@Override
	public void render(Graphics2D g) {
		if (pm.isPlayerDead()) {
			renderGameOver(g);
			return;
		}
		
		bg.render(g);
		
		g.setColor(fontColor);
		g.setFont(regularFont);
		g.drawString("Score: " + score.toString(), 10, 25);
		
		if (pm.getPlayer().recentlyHit) {
			if (! blinkToggle) {
				pm.render(g);
				blinkToggle = true;
			} else blinkToggle = false;
		} else {
			pm.render(g);
		}
		
		if (engageMode) {
			em.renderEnemy(g);
			akm.renderAllArrowKeys(g);
		}
		
		om.renderAllObstacles(g);
	}
	
	public void renderGameOver(Graphics2D g) {
		if (! pm.isPlayerDead()) return;

		bg.renderColor(g, Color.BLACK);
		dm.renderDialog(g, DialogManager.GAME_OVER);
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		if (engageMode) {
			akm.keyPressed(e);
		} else {
			pm.keyPressed(e);
		}
	}
	
	@Override
	public void keyReleased(KeyEvent e) {}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (pm.isPlayerDead()) {
			if (dm.getDialog(DialogManager.GAME_OVER)
				.reactIfButtonIsClicked(e))
				gameOverAudio.stopPlaying();
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == engageTimer) {
			if (! engageMode) return;
			pm.getPlayer()
				.damaged(em.getCurrentEnemy().getAtk());
			pm.burnPlayer();
		}
	}
	
	public Integer getScore() {
		return score;
	}
}
