package entities;

import java.awt.Graphics2D;

public abstract class GameEntity {
	protected double x;
	protected double y;
	protected double dx;
	protected double dy;
	
	public boolean isAlive;
	
	public GameEntity() {
		isAlive = true;
	}

	public abstract void update();
	public abstract void render(Graphics2D g);
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getDX() {
		return dx;
	}
	
	public double getDY() {
		return dy;
	}
	
	public void setPos(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public void setVelocity(double dx, double dy) {
		this.dx = dx;
		this.dy = dy;
	}
	
	public void move() {
		x += dx;
		y += dy;
	}
	
	public void accelerateX(double speedMult) {
		dx *= speedMult;
	}
	
	public void accelerateY(double speedMult) {
		dy *= speedMult;
	}
	
	public void kill(){
		isAlive = false;
	}
}