package entities;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class GameDialog extends Sprite {
	public static final double DIALOG_BUTTON_SCALE = 0.4;
	private ArrayList<GameLabel> labels;
	private ArrayList<GameButton> buttons;
	
	public GameDialog(String imgSrc) {
		super(imgSrc);
		
		labels = new ArrayList<GameLabel>();
		buttons = new ArrayList<GameButton>();
	}
	
	public void addLabel(String text, int x, int y) {
		GameLabel label = new GameLabel(text);
		label.setPos(this.x + x, this.y + y);
		
		labels.add(label);
	}
	
	public ArrayList<GameLabel> getLabels() {
		return labels;
	}
	
	public void clearLabels() {
		labels.clear();
	}
	
	public void addButton(GameButton btn) {
		btn.setPos(x + btn.x, y + btn.y);

		buttons.add(btn);
	}
	
	public boolean reactIfButtonIsClicked(MouseEvent e) {
		for (GameButton button : buttons) {
			if (button.isClicked(e)) {
				button.handle();
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public void render(Graphics2D g) {
		super.render(g);
		
		for (GameLabel label : labels)
			label.render(g);
		
		for (GameButton button : buttons)
			button.renderScaled(g);
	}
}