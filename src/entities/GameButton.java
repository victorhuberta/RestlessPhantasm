package entities;

import java.awt.Rectangle;
import java.awt.event.MouseEvent;

import utils.Method;

public class GameButton extends Sprite {
	private static final int CLICK_RECT_WIDTH = 2;
	private static final int CLICK_RECT_HEIGHT = 2;
	
	private Method method;
	
	public GameButton(String imgSrc) {
		super(imgSrc);
	}
	
	public GameButton(String imgSrc, Method method) {
		this(imgSrc);
		
		this.method = method;                                                  
	}
	
	public GameButton(String imgSrc, Method method,
		int x, int y, double scale) {

		this(imgSrc, method);
		
		this.x = x;
		this.y = y;
		this.scale = scale;
	}
	
	public boolean isClicked(MouseEvent e) {
		Rectangle click = new Rectangle(e.getX(), e.getY(), 
				CLICK_RECT_WIDTH, CLICK_RECT_HEIGHT);
		
		return getScaledBounds().intersects(click);
	}
	
	public Method getMethod() {
		return method;
	}
	
	public void setMethod(Method method) {
		this.method = method;
	}
	
	public void handle() {
		method.call();
	}
}