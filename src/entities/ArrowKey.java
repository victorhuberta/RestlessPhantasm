package entities;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ArrowKey extends Sprite {
	public static final int UP_KEY = 0;
	public static final int DOWN_KEY = 1;
	public static final int LEFT_KEY = 2;
	public static final int RIGHT_KEY = 3;
	
	public boolean isHighlighted;
	private int key;
	private BufferedImage highlightedImage;
	
	public ArrowKey(int key, String imgSrc) {
		super(imgSrc);
		isHighlighted = false;
		this.key = key;
	}
	
	@Override
	public void render(Graphics2D g) {
		if (isHighlighted)
			g.drawImage(highlightedImage, (int) x, (int) y, null);
		else
			super.render(g);
	}
	
	public int getKey() {
		return key;
	}
	
	public void setKey(int key) {
		this.key = key;
	}
	
	public void setHighlightedImgSrc(String imgSrc) {
		try {
			highlightedImage = ImageIO.read
				(getClass().getResourceAsStream(imgSrc));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}