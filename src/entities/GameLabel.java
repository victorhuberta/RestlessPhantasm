package entities;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

public class GameLabel extends GameEntity {
	
	private String text;
	private Font regularFont;
	private Color fontColor;
	
	public GameLabel(String text) {
		this.text = text;
		regularFont = new Font("Comic Sans", Font.BOLD, 20);
		fontColor = new Color(146, 40, 134);
	}

	@Override
	public void update() {}

	@Override
	public void render(Graphics2D g) {
		g.setColor(fontColor);
		g.setFont(regularFont);
		g.drawString(text, (int) x, (int) y);
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}

}