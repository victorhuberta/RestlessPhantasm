package entities;

import java.awt.Color;
import java.awt.Graphics2D;

import main.GamePanel;

public class Background extends Sprite {
	
	private double heightScale;

	public Background(String imgSrc) {
		super(imgSrc);
		
		scale = (double) GamePanel.WIDTH / image.getWidth();
		heightScale = (double) GamePanel.HEIGHT / image.getHeight();
	}
	
	@Override
	public void update() {
		if (x <= -GamePanel.WIDTH) x = 0;
		
		move();
	}
	
	@Override
	public void render(Graphics2D g) {
		g.drawImage(image, (int) x, (int) y,
			(int) (image.getWidth() * scale),
			(int) (image.getHeight() * heightScale), null);
		
		if (x < 0) {
			g.drawImage(image, (int) x + GamePanel.WIDTH,
				(int) y, (int) (image.getWidth() * scale),
				(int) (image.getHeight() * heightScale), null);
		}
	}
	
	
	public void renderColor(Graphics2D g, Color color) {
		g.setColor(color);
		g.fillRect((int) x, (int) y, GamePanel.WIDTH, GamePanel.HEIGHT);
	}

}