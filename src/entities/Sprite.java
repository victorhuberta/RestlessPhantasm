package entities;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Sprite extends GameEntity {
	protected double scale;
	protected String imgSrc;
	protected BufferedImage image;
	
	public Sprite(String imgSrc) {
		super();
		this.scale = 1;
		this.imgSrc = imgSrc;
		
		try {
			image = ImageIO.read
				(getClass().getResourceAsStream(imgSrc));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void update() {
		move();
	}

	@Override
	public void render(Graphics2D g) {
		g.drawImage(image, (int) x, (int) y, null);
	}
	
	public void renderScaled(Graphics2D g) {
		g.drawImage(image, (int) x, (int) y,
			(int) getScaledImageWidth(),
			(int) getScaledImageHeight(), null);
	}
	
	public boolean checkCollision(Sprite sprite) {
		return getScaledBounds().intersects(sprite.getScaledBounds());
	}
	
	private static final double COLLISION_SCALE = 0.5;
	
	public boolean checkCollisionWithScale(Sprite sprite) {
		Rectangle rect1 = getScaledBounds();
		rect1.setBounds
			((int) rect1.getX(), (int) rect1.getY(),
				(int) (rect1.getWidth() * COLLISION_SCALE),
					(int) (rect1.getHeight() * COLLISION_SCALE));
		
		Rectangle rect2 = sprite.getScaledBounds();
		rect2.setBounds
			((int) rect2.getX(), (int) rect2.getY(),
				(int) (rect2.getWidth() * COLLISION_SCALE),
					(int) (rect2.getHeight() * COLLISION_SCALE));
		
		return rect1.intersects(rect2);
		
	}
	
	public Rectangle getScaledBounds() {
		return new Rectangle((int) x, (int) y,
			(int) getScaledImageWidth(),
			(int) getScaledImageHeight());
	}
	
	public Rectangle getBounds() {
		return new Rectangle((int) x, (int) y,
			(int) getImageWidth(),
			(int) getImageHeight());
	}

	public String getImgSrc() {
		return imgSrc;
	}
	
	public double getScale() {
		return scale;
	}
	
	public void setScale(double scale) {
		this.scale = scale;
	}
	
	public double getImageWidth() {
		return image.getWidth();
	}
	
	public double getImageHeight() {
		return image.getHeight();
	}
	
	public double getScaledImageWidth() {
		return getImageWidth() * scale;
	}
	
	public double getScaledImageHeight() {
		return getImageHeight() * scale;
	}
}