package entities;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import utils.Animation;

public class VisualEffect extends Sprite {

	private Animation animation;
	
	public VisualEffect(String imgSrc) {
		super(imgSrc);
	}
	
	public VisualEffect(String baseName, String ext,
		int count, int initialFrameRepeat) {

		super(baseName + "1" + ext);
		
		ArrayList<String> imageTitles =
			Animation.getAllImageTitles(baseName, ext, count);
		
		animation = new Animation();
		animation.setFrames(imageTitles, initialFrameRepeat);
	}
	
	@Override
	public void update() {
		super.update();
		animation.updateCurrentFrame();
	}
	
	@Override
	public void render(Graphics2D g) {
		BufferedImage currentFrame = animation.getCurrentFrame();
		
		g.drawImage(currentFrame, (int) x, (int) y,
			(int) getImageWidth(),
			(int) getImageHeight(), null);
	}
	
	@Override
	public void renderScaled(Graphics2D g) {
		BufferedImage currentFrame = animation.getCurrentFrame();
		
		g.drawImage(currentFrame, (int) x, (int) y,
			(int) getScaledImageWidth(),
			(int) getScaledImageHeight(), null);
	}
	
	public Animation getAnimation() {
		return animation;
	}
	
}
