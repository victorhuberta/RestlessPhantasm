package entities;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import utils.Animation;

public class Enemy extends Sprite {

	private Animation animation;
	private int hp;
	private int atk;
	private boolean isFlipped;

	public Enemy(String imgSrc, int hp) {
		super(imgSrc);
		
		this.hp = hp;
		this.atk = 1;
		isFlipped = false;
	}
	
	@Override
	public void update() {
		super.update();
		
		if (animation != null)
			animation.updateCurrentFrame();
	}
	
	@Override
	public void render(Graphics2D g) {
		double scaledWidth = getScaledImageWidth();
		
		if (isFlipped)
			scaledWidth = -scaledWidth;
			
		g.drawImage
			(image, (int) x, (int) y,
			(int) scaledWidth,
			(int) getScaledImageHeight(), null);
	}
	
	public void initAnimation(String baseName, String ext,
		int count, int initialFrameRepeat) {

		ArrayList<String> imageTitles =
				Animation.getAllImageTitles(baseName, ext, count);
			
		animation = new Animation();
		animation.setFrames(imageTitles, initialFrameRepeat);
	}

	public void renderAnimation(Graphics2D g) {
		if (animation == null) return;
		
		double scaledWidth = getScaledImageWidth();
		BufferedImage currentFrame = animation.getCurrentFrame();
		
		if (isFlipped)
			scaledWidth = -scaledWidth;
			
		g.drawImage
			(currentFrame, (int) x, (int) y,
			(int) scaledWidth,
			(int) getScaledImageHeight(), null);
	}
	
	public int getHp() {
		return hp;
	}
	
	public void setHp(int hp) {
		this.hp = hp;
	}
	
	public int getAtk() {
		return atk;
	}
	
	public void setAtk(int atk) {
		this.atk = atk;
	}
	
	public void damaged(int dmg) {
		hp -= dmg;
		
		if (hp <= 0)
			isAlive = false;
	}
	
	public void setFlipped(boolean isFlipped) {
		this.isFlipped = isFlipped;
	}
	
	public boolean isFlipped() {
		return isFlipped;
	}
	
}