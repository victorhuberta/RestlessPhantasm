package managers;

import java.awt.Point;
import java.awt.Graphics2D;
import java.util.HashMap;

import entities.GameButton;
import entities.GameDialog;
import main.Game;
import states.PlayState;
import utils.Util;

public class DialogManager {
	public static final int GAME_OVER = 0;
	private static final String GAME_OVER_DLG_RES = "/dialogs/game_over.jpg";
	private static final String RESTART_BUTTON_RES = "/buttons/retry.png";
	private static final String CONTINUE_BUTTON_RES = "/buttons/menu.png";
	
	private HashMap<Integer, GameDialog> dialogSet;
	
	public DialogManager() {
		dialogSet = new HashMap<Integer, GameDialog>();
		
		initGameOverDialog();
	}
	
	private void initGameOverDialog() {
		GameDialog gameOverDlg = new GameDialog(GAME_OVER_DLG_RES);
		Point center = Util.getCenter(gameOverDlg.getImageWidth(), 
				gameOverDlg.getImageHeight());
		
		gameOverDlg.setPos(center.x, center.y);
		
		gameOverDlg.addButton(new GameButton(RESTART_BUTTON_RES, () -> {

			PlayState state = new PlayState(Game.PLAY_STATE);
			Game.gamePanel.resetState(state);
			Game.gamePanel.setStateById(Game.PLAY_STATE);

		}, 35, 300, GameDialog.DIALOG_BUTTON_SCALE));

		gameOverDlg.addButton(new GameButton(CONTINUE_BUTTON_RES, () -> {

			PlayState state = new PlayState(Game.PLAY_STATE);
			Game.gamePanel.resetState(state);	
			Game.gamePanel.setStateById(Game.MENU_STATE);

		}, 350, 300, GameDialog.DIALOG_BUTTON_SCALE));
		
		dialogSet.put(GAME_OVER, gameOverDlg);
	}
	
	public void updateDialog(int dlgCode) {
		GameDialog gameOverDlg = dialogSet.get(dlgCode);
		
		if (gameOverDlg.getLabels().isEmpty()) {
			PlayState playState = (PlayState)
					Game.gamePanel.getStateById(Game.PLAY_STATE);
			Integer score = playState.getScore();

			gameOverDlg.addLabel("Score: " + score.toString(), 170, 200);
		}
	}
	
	public void renderDialog(Graphics2D g, int dlgCode) {
		dialogSet.get(dlgCode).render(g);
	}
	
	public GameDialog getDialog(int dlgCode) {
		return dialogSet.get(dlgCode);
	}
}