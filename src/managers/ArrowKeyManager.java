package managers;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.*;

import entities.ArrowKey;
import main.GamePanel;
import utils.Util;

public class ArrowKeyManager {
	
	public static final int MAX_TOTAL_KEYS = 6;
	private static final int INITIAL_INC_DELAY = 5;
	
	private static final String UP_KEY_RES = "/arrow_keys/up_key.png";
	private static final String DOWN_KEY_RES = "/arrow_keys/down_key.png";
	private static final String LEFT_KEY_RES = "/arrow_keys/left_key.png";
	private static final String RIGHT_KEY_RES = "/arrow_keys/right_key.png";
	
	private static final String H_UP_KEY_RES = "/arrow_keys/h_up_key.png";
	private static final String H_DOWN_KEY_RES = "/arrow_keys/h_down_key.png";
	private static final String H_LEFT_KEY_RES = "/arrow_keys/h_left_key.png";
	private static final String H_RIGHT_KEY_RES = "/arrow_keys/h_right_key.png";
	
	private int totalKeys;
	private HashMap<Integer, String> keyAndRes;
	private HashMap<Integer, String> keyAndHRes;
	private ArrayList<ArrowKey> arrowKeys;
	private int currentIndex;
	private int keysIncDelay;
	
	private boolean isCompleted;
	
	public ArrowKeyManager() {
		totalKeys = 3;
		arrowKeys = new ArrayList<ArrowKey>();
		
		keyAndRes = new HashMap<Integer, String>();
		keyAndRes.put(ArrowKey.UP_KEY, UP_KEY_RES);
		keyAndRes.put(ArrowKey.DOWN_KEY, DOWN_KEY_RES);
		keyAndRes.put(ArrowKey.LEFT_KEY, LEFT_KEY_RES);
		keyAndRes.put(ArrowKey.RIGHT_KEY, RIGHT_KEY_RES);
		
		keyAndHRes = new HashMap<Integer, String>();
		keyAndHRes.put(ArrowKey.UP_KEY, H_UP_KEY_RES);
		keyAndHRes.put(ArrowKey.DOWN_KEY, H_DOWN_KEY_RES);
		keyAndHRes.put(ArrowKey.LEFT_KEY, H_LEFT_KEY_RES);
		keyAndHRes.put(ArrowKey.RIGHT_KEY, H_RIGHT_KEY_RES);
		
		currentIndex = 0;
		keysIncDelay = INITIAL_INC_DELAY;
		
		isCompleted = false;
		
		init();
	}
	
	public void init() {
		generateRandomKeys();
		setArrowKeysPos();
	}
	
	public void reset() {
		currentIndex = 0;
		isCompleted = false;
		arrowKeys.clear();
		init();
	}
	
	public void keysIncCountdown() {
		keysIncDelay--;
		
		if (keysIncDelay == 0) {
			if (totalKeys < ArrowKeyManager.MAX_TOTAL_KEYS)
				totalKeys++;
			keysIncDelay = INITIAL_INC_DELAY;
		}
	}
	
	public void generateRandomKeys() {
		for (int i = 0; i < totalKeys; i++) {
			int key = Util.randNumInRange
				(ArrowKey.UP_KEY, ArrowKey.RIGHT_KEY);

			ArrowKey arrowKey = new ArrowKey(key, keyAndRes.get(key));
			arrowKey.setHighlightedImgSrc(keyAndHRes.get(key));
			
			arrowKeys.add(arrowKey);
		}
	}
	
	public void setArrowKeysPos() {
		double mid = GamePanel.WIDTH / 2;
		double arrowKeyImageWidth = arrowKeys.get(0).getImageWidth();
		
		double x = mid - (totalKeys / 2 * arrowKeyImageWidth);
		
		if (totalKeys % 2 != 0)
			x -= (arrowKeyImageWidth / 2);
		
		for (int i = 0; i < arrowKeys.size(); i++) {
			arrowKeys.get(i).setPos(x + i * arrowKeyImageWidth, 400);
		}
	}

	public void keyPressed(KeyEvent e) {
		if (currentIndex >= arrowKeys.size()) {
			isCompleted = true;
			return;
		}
		
		int currentKey = arrowKeys.get(currentIndex).getKey();
		int pressedKey;
		
		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			pressedKey = ArrowKey.UP_KEY;
			break;
		case KeyEvent.VK_DOWN:
			pressedKey = ArrowKey.DOWN_KEY;
			break;
		case KeyEvent.VK_LEFT:
			pressedKey = ArrowKey.LEFT_KEY;
			break;
		case KeyEvent.VK_RIGHT:
			pressedKey = ArrowKey.RIGHT_KEY;
			break;
		default:
			return;
		}
		
		if (currentKey == pressedKey) {
			arrowKeys.get(currentIndex).isHighlighted = true;
			currentIndex++;
		} else {
			currentIndex = 0;
			for (ArrowKey arrowKey : arrowKeys)
				arrowKey.isHighlighted = false;
		}	
	}
	
	public void updateAllArrowKeys() {
		for (ArrowKey arrowKey : arrowKeys)
			arrowKey.update();
	}
	
	public void renderAllArrowKeys(Graphics2D g) {
		for (ArrowKey arrowKey : arrowKeys)
			arrowKey.render(g);
	}
	
	public int getTotalKeys() {
		return totalKeys;
	}
	
	public ArrayList<ArrowKey> getArrowKeys() {
		return arrowKeys;
	}
	
	public boolean hasCurrentSetBeenCompleted() {
		return isCompleted;
	}
}