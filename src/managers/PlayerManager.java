package managers;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;

import entities.Player;
import entities.VisualEffect;
import main.GamePanel;

public class PlayerManager {
	private static final String PLAYER_RES = "/players/witch.png";
	
	private static final int PLAYER_X = 150;
	private static final int PLAYER_Y = 450;
	
	private static final int ENGAGE_PLAYER_DEST_X = GamePanel.WIDTH / 4;
	private static final int ENGAGE_PLAYER_DEST_Y = GamePanel.HEIGHT / 2;
	
	private static final String EXPLOSION_BASE_RES =
			"/visual_effects/explosions/purple_explosion/explosion";
	private static final int EXPLOSION_RES_COUNT = 13;
	private static final String EXPLOSION_RES_EXT = ".png";
	
	private Player player;
	private boolean isExploded;
	private VisualEffect explosion;
	
	public PlayerManager() {
		player = new Player(PLAYER_RES);
		player.setPos(PLAYER_X, PLAYER_Y);
		
		isExploded = false;
		
		explosion = new VisualEffect
			(EXPLOSION_BASE_RES, EXPLOSION_RES_EXT,
				EXPLOSION_RES_COUNT, 1);
			
		explosion.setScale(0.5);
		// Magic numbers, because I don't care anymore.
		explosion.setPos(ENGAGE_PLAYER_DEST_X - 45,
			ENGAGE_PLAYER_DEST_Y - 65);
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public void burnPlayer() {
		isExploded = true;
	}
	
	public void update() {
		handleIfPlayerGoesOutsideOfBoundaries();
		player.update();
		
		if (isExploded) {
			explosion.update();

			if (explosion.getAnimation().getCurrentFrameIndex()
				>= EXPLOSION_RES_COUNT - 1) {
				isExploded = false;
				explosion.update();
			}
		}
		decreaseInvulDurationIfRecentlyHit();
	}
	
	public void render(Graphics2D g) {
		player.render(g);
		if (isExploded)
			explosion.renderScaled(g);
	}
	
	private static final int DISPLACEMENT = 6;

	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			player.setVelocity(0, -DISPLACEMENT);
			break;
		case KeyEvent.VK_DOWN:
			player.setVelocity(0, DISPLACEMENT);
			break;
		case KeyEvent.VK_LEFT:
			player.setVelocity(-DISPLACEMENT, 0);
			break;
		case KeyEvent.VK_RIGHT:
			player.setVelocity(DISPLACEMENT, 0);
			break;
		default:
			break;
		}
	}
	
	public boolean isPlayerDead() {
		return ! player.isAlive;
	}
	
	private static final int MIN_BOUNDARY_X = 5;
	private static final int MAX_BOUNDARY_X = GamePanel.WIDTH - 110;
	private static final int MIN_BOUNDARY_Y = 5;
	private static final int MAX_BOUNDARY_Y = GamePanel.HEIGHT - 95;
	
	public void handleIfPlayerGoesOutsideOfBoundaries() {
		double x = player.getX(), y = player.getY();
		double prevX = x, prevY = y;
		
		// Adjust player's x and y if they reach the boundaries.
		x = (x < MIN_BOUNDARY_X) ? MIN_BOUNDARY_X + 1 : x;
		x = (x > MAX_BOUNDARY_X) ? MAX_BOUNDARY_X- 1 : x;
		y = (y < MIN_BOUNDARY_Y) ? MIN_BOUNDARY_Y + 1 : y;
		y = (y > MAX_BOUNDARY_Y) ? MAX_BOUNDARY_Y - 1 : y;
		
		player.setPos(x, y);
		
		// If x or y changes, it means that player is at
		// one of the bounds. Set velocity to zero.
		if (prevX != x || prevY != y)
			player.setVelocity(0, 0);
	}
	
	// Set the invulnerability frame to three seconds
	private static final int INITIAL_INVUL_DURATION = 90;
	private int invulDuration = INITIAL_INVUL_DURATION;
	
	private void decreaseInvulDurationIfRecentlyHit() {
		if(player.recentlyHit) {
			invulDuration--;
			
			if(invulDuration <= 0) {
				player.recentlyHit = false;
				invulDuration = INITIAL_INVUL_DURATION;
			}
		}
	}

	private static final int DEST_MARGIN = 5;
	
	public void dragPlayerToSpecificPosition() {
		int newDX, newDY;
		
		if (player.getX() != ENGAGE_PLAYER_DEST_X)
			newDX = (player.getX() < ENGAGE_PLAYER_DEST_X) ? 5 : -5;
		else
			newDX = 0;
		
		if (player.getY() != ENGAGE_PLAYER_DEST_Y)
			newDY = (player.getY() < ENGAGE_PLAYER_DEST_Y) ? 5 : -5;
		else
			newDY = 0;
		
		player.setVelocity(newDX, newDY);
	}
	
	public void adjustPlayerPosIfWithinRangeFromDest() {
		if (player.getX() >= (ENGAGE_PLAYER_DEST_X - DEST_MARGIN) 
			&& player.getX() <= (ENGAGE_PLAYER_DEST_X + DEST_MARGIN)) {
			
			player.setVelocity(0, player.getDY());
			player.setPos(ENGAGE_PLAYER_DEST_X, player.getY());
		}
		
		if (player.getY() >= (ENGAGE_PLAYER_DEST_Y - DEST_MARGIN) 
			&& player.getY() <= (ENGAGE_PLAYER_DEST_Y + DEST_MARGIN)) {
			
			player.setVelocity(player.getDX(), 0);
			player.setPos(player.getX(), ENGAGE_PLAYER_DEST_Y);
		}
	}
}