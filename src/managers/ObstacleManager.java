package managers;

import java.awt.Graphics2D;
import java.util.ArrayList;

import entities.FloatObstacle;
import entities.GroundObstacle;
import entities.Obstacle;
import entities.Player;
import entities.VisualEffect;
import utils.Animation;
import utils.Util;

public class ObstacleManager {
	private static final String OBSTACLE_RES = "/obstacles/floating_eyebeast.png";
	private static final String EXPLOSION_BASE_RES =
		"/visual_effects/explosions/purple_explosion/explosion";
	private static final int EXPLOSION_RES_COUNT = 13;
	private static final String EXPLOSION_RES_EXT = ".png";
	
	private static final int GROUND_OBSTACLE = 1;
	private static final int FLOAT_OBSTACLE = 2;
	
	private static final int MIN_SPAWN_DELAY = 30;
	private static final int MAX_SPAWN_DELAY = 90;
	
	private static final int SPAWN_POINT_X = 1600;
	private static final int SPAWN_POINT_Y = 450;
	private static final int OBSTACLE_DESTRUCTION_X = 20;
	private static final int INITIAL_OBSTACLE_VELOCITY_X = -5;
	
	private int spawnDelay;
	public static ArrayList<Obstacle> obstacles;
	public static ArrayList<VisualEffect> explosions;
	
	public ObstacleManager() {
		spawnDelay = 0;
		obstacles = new ArrayList<Obstacle>();
		explosions = new ArrayList<VisualEffect>();
	}
	
	public ArrayList<Obstacle> getObstacles() {
		return obstacles;
	}
	
	public void spawnObstaclesWhenItIsTime() {
		spawnDelay--;
		
		if(spawnDelay <= 0) {
			spawnObstacle();
			spawnDelay = Util.randNumInRange(MIN_SPAWN_DELAY, MAX_SPAWN_DELAY);
		}
	}
	
	private void spawnObstacle() {
		
		int obstType = Util.randNumInRange(GROUND_OBSTACLE, FLOAT_OBSTACLE);
		
		Obstacle nObstacle = null;
		
		switch(obstType){
		case GROUND_OBSTACLE:
			nObstacle = new GroundObstacle(OBSTACLE_RES);
			nObstacle.setPos(SPAWN_POINT_X, SPAWN_POINT_Y);
			break;
		case FLOAT_OBSTACLE:
			nObstacle = new FloatObstacle(OBSTACLE_RES);
			nObstacle.setPos(SPAWN_POINT_X, Util.randNumInRange(50, 350));
			break;
		default: 
			break;
		}
		
		nObstacle.setScale(0.1);
		nObstacle.setVelocity(INITIAL_OBSTACLE_VELOCITY_X, 0);
		obstacles.add(nObstacle);
	}
	
	public void updateObstaclesAndCheckCollision(Player player) {
		for (Obstacle o : obstacles) {
			o.update();
			
			if(o.checkCollisionWithScale(player) && ! player.recentlyHit) {
				player.recentlyHit = true;
				player.damaged(1);
				o.kill();

				VisualEffect explosion = new VisualEffect
					(EXPLOSION_BASE_RES, EXPLOSION_RES_EXT,
						EXPLOSION_RES_COUNT, 2);
				
				explosion.setScale(0.5);
				// Magic numbers, because I don't care anymore.
				explosion.setPos(o.getX() - 45, o.getY() - 65);
				explosion.setVelocity(o.getDX(), o.getDY());
				explosions.add(explosion);
			}
			
			if(o.getX() <= OBSTACLE_DESTRUCTION_X)
				o.kill();
		}
		
		for (int i = 0; i < explosions.size(); i++) {
			VisualEffect explosion = explosions.get(i);
			Animation animation = explosion.getAnimation();
			
			
			if (animation.getCurrentFrameIndex()
				>= EXPLOSION_RES_COUNT - 1)
				explosions.remove(i);
			else
				explosion.update();
		}
	}
	
	public void killAllObstacles() {
		for (Obstacle obstacle : obstacles)
			obstacle.kill();
	}
	
	public void removeDeadObstacles() {
		for (int i = 0; i< obstacles.size(); i++){
			Obstacle o = obstacles.get(i);
			
			if(! o.isAlive)
				obstacles.remove(i);
		}
	}
	
	public void accelerateObstaclesBy(double speedMult) {
		for (Obstacle o : obstacles)
			o.accelerateX(speedMult);
	}
	
	public void renderAllObstacles(Graphics2D g) {
		for (Obstacle o : obstacles)
			o.renderScaled(g);
		
		for (VisualEffect explosion : explosions)
			explosion.renderScaled(g);
	}
}